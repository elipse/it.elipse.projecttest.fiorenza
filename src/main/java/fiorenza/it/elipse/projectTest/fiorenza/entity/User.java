package fiorenza.it.elipse.projectTest.fiorenza.entity;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="users")
public class User {
	// define fields
	
		@Id
		@GeneratedValue(strategy=GenerationType.IDENTITY)
		@Column(name="id")
		private int id;
		
		@Column(name="first_name")
		private String firstName;
		
		@Column(name="last_name")
		private String lastName;
		
		@Column(name="email")
		private String email;

		@Column(name="password")
		private String password;
		
		@Column(name="role")
		private String role;
		
		@Column(name="create_date")
		private java.sql.Timestamp createDate;
		
		@Column(name="create_user")
		private String createUser;
		
		@Column(name="update_date")
		private java.sql.Timestamp updateDate;
		
		@Column(name="update_user")
		private String updateUser;
		
		@Column(name="active")
		private boolean active;

		@Column(name="reset_password_token")
		private String resetPasswordToken;
		
		
		// define constructor
		
		public User() {
			
		}


		public User(String firstName, String lastName, String email, String password, String role, Timestamp createDate,
				String createUser, Timestamp updateDate, String updateUser, boolean active, String resetPasswordToken) {
			this.firstName = firstName;
			this.lastName = lastName;
			this.email = email;
			this.password = password;
			this.role = role;
			this.createDate = createDate;
			this.createUser = createUser;
			this.updateDate = updateDate;
			this.updateUser = updateUser;
			this.active = active;
			this.resetPasswordToken = resetPasswordToken;
		}




		public int getId() {
			return id;
		}



		public void setId(int id) {
			this.id = id;
		}



		public String getFirstName() {
			return firstName;
		}



		public void setFirstName(String firstName) {
			this.firstName = firstName;
		}



		public String getLastName() {
			return lastName;
		}



		public void setLastName(String lastName) {
			this.lastName = lastName;
		}



		public String getEmail() {
			return email;
		}



		public void setEmail(String email) {
			this.email = email;
		}



		public String getPassword() {
			return password;
		}



		public void setPassword(String password) {
			this.password = password;
		}



		public String getRole() {
			return role;
		}



		public void setRole(String role) {
			this.role = role;
		}



		public java.sql.Timestamp getCreateDate() {
			return createDate;
		}



		public void setCreateDate(java.sql.Timestamp createDate) {
			this.createDate = createDate;
		}



		public String getCreateUser() {
			return createUser;
		}



		public void setCreateUser(String createUser) {
			this.createUser = createUser;
		}



		public java.sql.Timestamp getUpdateDate() {
			return updateDate;
		}



		public void setUpdateDate(java.sql.Timestamp updateDate) {
			this.updateDate = updateDate;
		}



		public String getUpdateUser() {
			return updateUser;
		}



		public void setUpdateUser(String updateUser) {
			this.updateUser = updateUser;
		}



		public boolean isActive() {
			return active;
		}



		public void setActive(boolean active) {
			this.active = active;
		}


		public String getResetPasswordToken() {
			return resetPasswordToken;
		}



		public void setResetPasswordToken(String resetPasswordToken) {
			this.resetPasswordToken = resetPasswordToken;
		}




		@Override
		public String toString() {
			return "User [id=" + id + ", firstName=" + firstName + ", lastName=" + lastName + ", email=" + email
					+ ", password=" + password + ", role=" + role + ", createDate=" + createDate + ", createUser="
					+ createUser + ", updateDate=" + updateDate + ", updateUser=" + updateUser + ", active=" + active
					+ ", resetPasswordToken=" + resetPasswordToken + "]";
		}


		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + (active ? 1231 : 1237);
			result = prime * result + ((createDate == null) ? 0 : createDate.hashCode());
			result = prime * result + ((createUser == null) ? 0 : createUser.hashCode());
			result = prime * result + ((email == null) ? 0 : email.hashCode());
			result = prime * result + ((firstName == null) ? 0 : firstName.hashCode());
			result = prime * result + id;
			result = prime * result + ((lastName == null) ? 0 : lastName.hashCode());
			result = prime * result + ((password == null) ? 0 : password.hashCode());
			result = prime * result + ((resetPasswordToken == null) ? 0 : resetPasswordToken.hashCode());
			result = prime * result + ((role == null) ? 0 : role.hashCode());
			result = prime * result + ((updateDate == null) ? 0 : updateDate.hashCode());
			result = prime * result + ((updateUser == null) ? 0 : updateUser.hashCode());
			return result;
		}


		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			User other = (User) obj;
			if (active != other.active)
				return false;
			if (createDate == null) {
				if (other.createDate != null)
					return false;
			} else if (!createDate.equals(other.createDate))
				return false;
			if (createUser == null) {
				if (other.createUser != null)
					return false;
			} else if (!createUser.equals(other.createUser))
				return false;
			if (email == null) {
				if (other.email != null)
					return false;
			} else if (!email.equals(other.email))
				return false;
			if (firstName == null) {
				if (other.firstName != null)
					return false;
			} else if (!firstName.equals(other.firstName))
				return false;
			if (id != other.id)
				return false;
			if (lastName == null) {
				if (other.lastName != null)
					return false;
			} else if (!lastName.equals(other.lastName))
				return false;
			if (password == null) {
				if (other.password != null)
					return false;
			} else if (!password.equals(other.password))
				return false;
			if (resetPasswordToken == null) {
				if (other.resetPasswordToken != null)
					return false;
			} else if (!resetPasswordToken.equals(other.resetPasswordToken))
				return false;
			if (role == null) {
				if (other.role != null)
					return false;
			} else if (!role.equals(other.role))
				return false;
			if (updateDate == null) {
				if (other.updateDate != null)
					return false;
			} else if (!updateDate.equals(other.updateDate))
				return false;
			if (updateUser == null) {
				if (other.updateUser != null)
					return false;
			} else if (!updateUser.equals(other.updateUser))
				return false;
			return true;
		}

}
