package fiorenza.it.elipse.projectTest.fiorenza.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import fiorenza.it.elipse.projectTest.fiorenza.entity.User;

public interface UserRepository extends JpaRepository<User, Integer> {

	 @Query("SELECT u FROM User u WHERE u.email = ?1")
	    public User findByEmail(String email); 
	     
	    public User findByResetPasswordToken(String token);

}
