package fiorenza.it.elipse.projectTest.fiorenza;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;


@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

	// add a reference to our security data source
	
	@Autowired
	  private DataSource dataSource;
	
	@Autowired
	public void configAuthentication(AuthenticationManagerBuilder authBuilder)throws Exception {
		authBuilder.jdbcAuthentication()
			.dataSource(dataSource)
			.passwordEncoder(new BCryptPasswordEncoder())
			.usersByUsernameQuery("select email, password, active from users where email=?")
			.authoritiesByUsernameQuery("select email, role from users where email=?")
			;
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		  http.authorizeRequests()
		  .antMatchers("/login").permitAll()
		  .anyRequest().authenticated()
	       .and()
	       .formLogin().permitAll()
	       .and()
	       .logout().permitAll()
	       ;
	}
	 
	
	
	}
		
