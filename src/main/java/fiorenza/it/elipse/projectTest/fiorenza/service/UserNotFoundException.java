package fiorenza.it.elipse.projectTest.fiorenza.service;

public class UserNotFoundException extends Exception {

	public UserNotFoundException(String message) {
		super(message);
		
	}

}
