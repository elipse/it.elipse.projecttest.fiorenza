package fiorenza.it.elipse.projectTest.fiorenza;

import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;



public class ServletInitializer extends  SpringBootServletInitializer {
	// extends AbstractAnnotationConfigDispatcherServletInitializer
	
	
	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(Application.class);
	}
	
	
}
