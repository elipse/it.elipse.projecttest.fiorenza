package fiorenza.it.elipse.projectTest.fiorenza.service;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import fiorenza.it.elipse.projectTest.fiorenza.dao.UserRepository;
import fiorenza.it.elipse.projectTest.fiorenza.entity.User;

@Service
@Transactional
public class UserService {

	@Autowired
    private UserRepository userRepo;
     
 
    public void updateResetPasswordToken(String token, String email) throws UserNotFoundException {
        User user = userRepo.findByEmail(email);
        
        if (user != null) {
        	user.setResetPasswordToken(token);
            userRepo.save(user);
        } else {
        	 throw new UserNotFoundException("Could not find any user with the email " + email);
        }
    }
    
    public User get(String resetPasswordToken) {
        return userRepo.findByResetPasswordToken(resetPasswordToken);
    } 
    
    public void updatePassword(User user, String newPassword) {
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        String encodedPassword = passwordEncoder.encode(newPassword);
        
        user.setPassword(encodedPassword);
        user.setResetPasswordToken(null);
        
        userRepo.save(user);
    }
}